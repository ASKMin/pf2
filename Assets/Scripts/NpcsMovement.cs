﻿using UnityEngine;
using UnityEngine.AI;
using System.Collections;


public class NpcsMovement : MonoBehaviour {

	public Transform[] pointss;
	private int destPoint = 0;
	private NavMeshAgent agent1;



	void Start () {


		this.agent1 = GetComponent<NavMeshAgent>();

		// Disabling auto-braking allows for continuous movement
		// between points (ie, the agent doesn't slow down as it
		// approaches a destination point).
		this.agent1.autoBraking = false;

		GotoNextPoint();
	}


	void GotoNextPoint() {

		// Returns if no points have been set up
		if (pointss.Length == 0)
			return;

		// Set the agent to go to the currently selected destination.
		this.agent1.destination = pointss[destPoint].position;

		// Choose the next point in the array as the destination,
		// cycling to the start if necessary.
		destPoint = (destPoint + 1) % pointss.Length;
	}


	void Update () {
		// Choose the next destination point when the agent gets
		// close to the current one.
		if (!this.agent1.pathPending && this.agent1.remainingDistance < 0.5f)
			GotoNextPoint();
	}
}